.#!/bin/bash

sudo apt-get remove -y geary simple-scan

sudo apt-get update
echo "y" | sudo apt-get upgrade
sudo apt-get autoremove -y

sudo apt-get install -y vlc software-properties-common firefox guake flashplugin-installer git

mkdir ~/.gconf
mkdir ~/.gconf/apps
mkdir ~/.gconf/apps
mkdir ~/.gconf/apps/guake
mkdir ~/.gconf/apps/guake/
mkdir ~/.gconf/apps/guake/keybindings/global/
mkdir ~/.gconf/apps/guake/keybindings/local/

cp %gconf.xml ~/.gconf/apps/guake/keybindings/global/%gconf.xml
cp %lconf.xml ~/.gconf/apps/guake/keybindings/local/%gconf.xml

grep "rfkill block bluetooth" /etc/rc.local
if [ $? -ne 0 ];
then
    echo "rfkill block bluetooth" | sudo tee -a /etc/rc.local 
fi

#Create list added repositories to OS
MY_REPOSITORIES=$(
for APT in `find /etc/apt/ -name \*.list`; do
    grep -o "^deb http://ppa.launchpad.net/[a-z0-9\-]\+/[a-z0-9\-]\+" $APT | while read ENTRY ; do
        USER=`echo $ENTRY | cut -d/ -f4`
        PPA=`echo $ENTRY | cut -d/ -f5`
        echo sudo apt-add-repository ppa:$USER/$PPA
    done
done
)

#Sublime3
FLAG=$(echo "$MY_REPOSITORIES" | grep 'ppa:webupd8team/sublime-text-3' )
if [ $? -eq 1 ];
then
    sudo add-apt-repository -y ppa:webupd8team/sublime-text-3
    sudo apt-get update
    sudo apt-get install -y sublime-text-installer
fi
FLAG=$(dpkf -s sublime-text-installer)
if [ $? -eq 1 ];
then
    mkdir ~/.config/sublime-text-3
    mkdir ~/.config/sublime-text-3/Packages
    mkdir ~/.config/sublime-text-3/Packages/User
    cp Default\ \(Linux\).sublime-keymap ~/.config/sublime-text-3/Packages/User/
fi
    


#For Laptop : Install TLP for Improve Battery Life and Reduce Overheating
FLAG=$(echo "$MY_REPOSITORIES" | grep 'ppa:linrunner/tlp' )
if [ $? -eq 1 ];
then
    sudo add-apt-repository -y ppa:linrunner/tlp
    sudo apt-get update
    sudo apt-get install -y tlp tlp-rdw
    sudo tlp start
fi

echo "System will reboot in 60sec ..."
sudo shutdown -r 1

#PyCharm
FLAG=$(echo "$MY_REPOSITORIES" | grep 'ppa:linrunner/tlp' )
if [ $? -eq 1 ];
	then
	sudo add-apt-repository -y ppa:mystic-mirage/pycharm
	sudo apt-get update
	sudo apt-get install -y pycharm
fi
